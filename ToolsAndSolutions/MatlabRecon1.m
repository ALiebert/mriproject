filename=uigetfile('*.mat',  'All Files (*.*)','MultiSelect','on')

for current_filename=1:length(filename)
    filename{current_filename}
    data=open(filename{current_filename})
    MultiRaid=data.MultiRaid;
    raw_data_unshaped=MultiRaid.rawdata;
    NumberOfChannels=MultiRaid.sMDH.ulChannelId;
    NumberOfSlices=max(MultiRaid.loopcounters(:,3));
    
    %number_of_lines=length(raw_data_unshaped)/number_of_channels;
    NumberOfLines=max(MultiRaid.loopcounters(:,1));
    result_k_space=zeros(NumberOfSlices,NumberOfChannels,NumberOfLines,748);
    result_image=zeros(NumberOfSlices,NumberOfChannels,NumberOfLines,748);
    
    
    
    for slice=1:NumberOfSlices
        for channel=1:NumberOfChannels
            for line=1:NumberOfLines
                [slice,channel,line];
                PositionInRawData=(line-1)*NumberOfChannels*NumberOfSlices+(slice-1)*NumberOfChannels+channel;
                result_k_space(slice,channel,line,:)=raw_data_unshaped(PositionInRawData,:);
                %result_k_space(slice,line,:)=raw_data_unshaped(NumberOfChannels*slice+(line-1)*NumberOfSlices,:);
            end
            result_image(slice,channel,:,:)=fftshift(fft2(squeeze(result_k_space(slice,channel,:,:))));
            signal_values(current_filename,slice,channel,:,:)=abs(result_image(slice,channel,:,:));
        end
    end
end

%SoS combine



tr=[99,200,400,800,1600,3200,4800];
signal_values_size=size(signal_values)
t1_image=zeros(signal_values_size(2:end));
for slice=1:signal_values_size(2)
    for channels=1:signal_values_size(3)
        for pixel_x=1:signal_values_size(5)
            for pixel_y=1:signal_values_size(4)
                signal_to_fit=signal_values(:,slice,channels,pixel_y,pixel_x);
                [xData, yData] = prepareCurveData( tr,signal_to_fit);
                
                ft = fittype( 'Mz*(1-exp(-x/t_1))', 'independent', 'x', 'dependent', 'y' );
                opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
                opts.Display = 'Off';
                opts.Lower = [0 0];
                opts.StartPoint = [0.546881519204984 4000];
                opts.Upper = [1 100000];
                [fitresult, gof] = fit( xData, yData, ft, opts );
                t1_image(slice,channels,pixel_y,pixel_x)=fitresult.t_1;
            end
        end
    end
end
%signal_to_fit=signal_values(:,)


%[xData, yData] = prepareCurveData( tr, signal_to_fit );

%ft = fittype( 'Mz*(1-exp(-x/t_1))', 'independent', 'x', 'dependent', 'y' );
%opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
%opts.Display = 'Off';
%opts.StartPoint = [0.546881519204984 4000];

%[fitresult, gof] = fit( xData, yData, ft, opts );







