##################################################################################################################
#Code by Andrzej Liebert
#
#MRI-Project teach Image Reconstuction Program
#
#
#
##########################################
import numpy
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.widgets import Slider
import scipy.io as sio
import twix_parser
import twix_matfile
import tkinter as tk
from tkinter import filedialog

root = tk.Tk()
root.withdraw()

file_path = filedialog.askopenfilenames()
file_names=root.tk.splitlist(file_path)

image_set=[]
for file_number in range(0,len(file_names)):
    data=twix_matfile.read(file_names[file_number])
    result_image=twix_matfile.reconstruct(data)
    result_image_combined=twix_matfile.SoS_combine(result_image)
    image_set.append(result_image_combined)
single_image=image_set[0]

#print(image_set.shape)
signal_values=numpy.zeros(shape=[single_image.shape[3],single_image.shape[4],len(image_set)])
for image_number in range(0,len(image_set)):
    single_image=image_set[image_number]
    print(single_image.shape)
    for acq in range(0,single_image.shape[0]):
        for slice in range(0,single_image.shape[1]):
            for echo in range(0,single_image.shape[2]):
 #               for channel in range(0,single_image.shape[3]):
                    for line in range(0,single_image.shape[3]):
                        for sample in range(0,single_image.shape[4]):
                            signal_values[line,sample,image_number]=numpy.abs(single_image[acq,slice,echo,line,sample])

single_image=image_set[0]
tr_values=[99,200,400,800,1600,3200,4800]

SignalForFit=signal_values[190,380,:]

#twix_matfile.t1_fit(signal_values,tr_values)
abs_single_image=numpy.abs(single_image[0,11,0,:,:])
fig, ax = plt.subplots()
plt.subplots_adjust(left=0.25, bottom=0.25)
img=plt.imshow(abs_single_image, cmap='gray', norm=LogNorm())

axcolor = 'lightgoldenrodyellow'
axslice = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor=axcolor)

sslice = Slider(axslice, 'Slice', 1, 12, valinit=12,valfmt='%0.0f')

def update(val):
    slice_number = int(sslice.val)
    img.set_data(numpy.abs(single_image[0,slice_number-1,0,:,:]))
sslice.on_changed(update)
plt.show()



