##################################################################################################################
#Code by Andrzej Liebert
#
#MRI-Project teach Image Reconstuction Program
#
#
#
#
import numpy
from matplotlib import pyplot as plt
from matplotlib.colors import LogNorm
import scipy.io as sio
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import twix_parser
class TWIX_DATA():
    def __init__(self):
        self.used_channels = 0  # ushUsedChannels
        self.line = 0  # ushLine
        self.acquisition = 0  # ushAcquisition
        self.slice = 0  # ushSlice
        self.partition = 0  # ushPartition
        self.echo = 0  # ushEcho
        self.phase = 0  # ushPhase
        self.repitition = 0  # ushRepetition
################################################BASIC READ IN OF MATFILE################################################
def read(filename):
    raiddata = sio.loadmat(filename, struct_as_record=False,squeeze_me=True)

    MultiRaid=raiddata['MultiRaid']
######################DETERMINATION IN WHICH column you can find the information about line,
    NumberOfLines=int(numpy.max(MultiRaid.loopcounters[:,0]))
    NumberOfAcquistions=int(numpy.max(MultiRaid.loopcounters[:,1]))
    NumberOfSlices=int(numpy.max(MultiRaid.loopcounters[:,2]))
    NumberOfEchos=int(numpy.max(MultiRaid.loopcounters[:,4]))
    NumberOfChannels=int(numpy.max(MultiRaid.loopcounters[:,14]))
    NumberOfSamplesInLine=int(numpy.max(MultiRaid.loopcounters[:,15]))
###############################################DEBUG SHOW WHATS GOING ON################################################
    print(NumberOfAcquistions)
    print(NumberOfSlices)
    print(NumberOfEchos)
    print(NumberOfLines+1)
    print(NumberOfChannels)
    print(NumberOfSamplesInLine)
########################################################################################################################

    rawdata_unshaped=MultiRaid.rawdata #read in rawdata, it comes in form of a NxSamples array

    #print(rawdata_unshaped[9143,0])
    #calculate the number of samples in a single acquisition

    #print(SamplesInAcquistion)
    #Number of Samples in a Slice
    #SamplesInSlice=int(len(rawdata_unshaped[:,0])/NumberOfSlices/NumberOfChannels)



###############################RESHAPING INTO ACQxSLICESxECHOxLINESxSAMPLES ARRAY########################################
#               reschape of the data into reasonable form. At this point it might be necessary to                       #
#                                   change this for a radial reconstruction                                             #
#                             The position in the main rawdata array can be found as                                    #
#   CURRENT_ACQ*CURRENT_LINE*NUMBER_OF_SLICES*NUMBER_OF_CHANNELS+CURRENT_SLICE*NUMBER_OF_CHANNELS+CURRENT_CHANNEL       #
#########################################################################################################################
    data_reshaped= numpy.zeros(shape=(NumberOfAcquistions,NumberOfSlices,NumberOfEchos,NumberOfChannels,NumberOfLines,
                                  NumberOfSamplesInLine),dtype=complex)
    print(data_reshaped.shape)
    #print(SamplesInSlice)
    for acq in range (0,NumberOfAcquistions):
        for slice in range(0, NumberOfSlices):
            for echo in range(0, NumberOfEchos):
                for line in range(0, NumberOfLines):
                    for channel in range(0,NumberOfChannels): #WATCH THE CHANNELS
                        PositionInRawData = (line)*(acq+1)*(NumberOfChannels)*(NumberOfSlices)+(slice)*NumberOfChannels+channel
                        #print(acq,slice,echo,line,channel,PositionInRawData)#DEBUG show position in the rawdata_unshaped
                        data_reshaped[acq, slice,echo ,channel, line,:] = rawdata_unshaped[PositionInRawData, :]
    return data_reshaped,NumberOfAcquistions,NumberOfSlices,NumberOfEchos,NumberOfChannels,NumberOfLines,NumberOfSamplesInLine

def reconstruct(data):
################################################RECONSTRUCTION LOOP######################################################
#Takes in data in form of an multidimensional array in form [Acq,Slice,Echo,Channel,Line,Sample] and reconstructs into  #
#an image in form [Acq,Slice,Echo,Channel,Line,Sample] but in the image domain. Reconstruction is performed through a   #
# 2D FFT without any additional parts                                                                                   #
#########################################################################################################################
    data_reshaped=data[0]
    NumberOfAcquistions = data[1]
    NumberOfSlices = data[2]
    NumberOfEchos = data[3]
    NumberOfChannels = data[4]
    NumberOfLines = data[5]
    NumberOfSamplesInLine = data[6]
    result_image=numpy.zeros(shape=(NumberOfAcquistions,NumberOfSlices,NumberOfEchos,NumberOfChannels,NumberOfLines,NumberOfSamplesInLine),dtype=complex)
    result_full_k_space=numpy.zeros(shape=(NumberOfAcquistions,NumberOfSlices,NumberOfEchos,NumberOfChannels,NumberOfLines,NumberOfSamplesInLine),dtype=complex)
    for acq in range (0,NumberOfAcquistions):
        for slice in range (0, NumberOfSlices):
            for echo in range(0, NumberOfEchos):
                for channel in range (NumberOfChannels, 0,-1): #REDO CHANNELS!!
                    single_k_space=numpy.squeeze(data_reshaped[acq,slice,echo,NumberOfChannels-channel,:,:])
                    result_full_k_space[acq,slice,echo,NumberOfChannels-channel,:,:]=single_k_space
                    result_image[acq,slice,echo,NumberOfChannels-channel,:,:]=numpy.fft.fftshift(numpy.fft.fft2(single_k_space))
    return result_image
################################################Sum of Squares Combine##################################################
#                                       This function take data in the shape
#                                   Data=[Acq,Slice,Echo,Channel,Line,SamplesInLine]
#                                           and returns a matrix in form
#                                    Result=[Acq,Slice,Echo,Line,SamplesInLine]
#                   in which the entries are a result of a sum of squares combination of the singular channels
########################################################################################################################
def SoS_combine(data):
    print(data.shape)
    NumberOfAcquistions = data.shape[0]
    NumberOfSlices = data.shape[1]
    NumberOfEchos = data.shape[2]
    NumberOfChannels = data.shape[3]
    NumberOfLines = data.shape[4]
    NumberOfSamplesInLine = data.shape[5]
    result_image=numpy.zeros(shape=(NumberOfAcquistions,NumberOfSlices,NumberOfEchos,NumberOfLines,NumberOfSamplesInLine))
    for acq in range (0,NumberOfAcquistions):
        for slice in range (0, NumberOfSlices):
            for echo in range(0, NumberOfEchos):
                for line in range(0, NumberOfLines):
                    for channel in range(0,NumberOfChannels):
                        result_image[acq,slice,echo,line,:]=result_image[acq,slice,echo,line,:]+numpy.square(numpy.abs(data[acq,slice,echo,channel,line,:]))
                    result_image[acq,slice,echo,line,:]=numpy.sqrt(result_image[acq,slice,echo,line,:])
    return result_image





def t1_function(x,t1,a):
    return a*(1-numpy.exp(-x/t1))

def t1_fit(signal_values,tr_values):
    popt=numpy.empty(shape=[signal_values.shape[0],signal_values.shape[1]])
    for line in range(signal_values.shape[0]):
        for sample in range(signal_values.shape[1]):
            SignalForFit=signal_values[line,sample,:]
            popt[line,sample], pcov=curve_fit(t1_function,tr_values,SignalForFit)
        img = plt.imshow(popt(0), cmap='gray', norm=LogNorm())
        plt.show()


#final_image=numpy.zeros(shape=(NumberOfAcquistions,NumberOfSlices,NumberOfEchos,NumberOfLines,NumberOfSamplesInLine))
#for channel in range (NumberOfChannels,0,-1):
    #final_image[0,8,0,:,:]=final_image+numpy.square(result_image[0,8,0,NumberOfChannels-channel,:,:])
#plt.imshow(numpy.abs(result_full_k_space[0,0,0,0]))
#plt.imshow()

##################################SENSE correction RAW DATA!!!##########################################################
#   Some raw data might be taken with SENSE that is the data from the different channels is interleaved in the k-space
#   the data in k-space is ordered as  ---channel 1----
#                                      ---channel 2----
#                                      ---channel 1----
#                                      ---channel 2----
#   and so on hence we need to order it properly and 1st than do the 2D FFT
#
#
#final_image=numpy.zeros(shape=(762,748),dtype=complex)
#final_image=numpy.fft.fftshift(numpy.fft.fft2(single_k_space))
#final_k_space=numpy.zeros(shape=(762,748),dtype=complex)
#for acq in range (0,NumberOfAcquistions):
#    for slice in range (0, NumberOfSlices):
#        for echo in range(0, NumberOfEchos):
#            for line in range(0,NumberOfLines):
#                final_k_space[2*line]=result_full_k_space[0,8,0,0,line,:]
#                final_k_space[2*line-1]=result_full_k_space[0,8,0,1,line,:]
#final_image=numpy.fft.fftshift(numpy.fft.fft2(single_k_space))
#final_image=numpy.square(result_image[0,8,0,0,:,:])+num# py.square(result_image[0,8,0,1,:,:])
#plt.imshow(numpy.abs(final_image),cmap='gray',norm=LogNorm())
#plt.show()


